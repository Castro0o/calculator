# Simple Calculator API

## run from docker image

Decompress .gzip

`gzip -d calculator_api.tar.gz`


Load image file into local docker images repository

`docker load -i calculator_api.tar`


Run container

`docker run --rm -p 6543:6543 --name calculator_api  calculator_api:latest`


Find API documentation at:

http://0.0.0.0:6543/documentation


Send request with curl calculating `10 + 1`:

``` 
curl -X POST -H "Content-type: application/json" \
-H "Accept: application/json" \
-d '{"input":"10 + 1"}' \
http//0.0.0.0:6543/calculate
```


Shutdown the docker container gracefully:

`docker stop calculator_api`

