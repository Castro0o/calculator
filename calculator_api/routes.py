import os


def includeme(config):
    config.include("pyramid_openapi3")
    config.pyramid_openapi3_spec(
        os.path.join(os.path.dirname(__file__), "openapi.yaml"),
        route='/documentation/openapi.yaml'
    )
    config.pyramid_openapi3_add_explorer(route='/documentation')
    config.include("pyramid_openapi3")

    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_route('home', '/')
    config.add_route('calculate', '/calculate')
