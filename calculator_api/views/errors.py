from pyramid.view import notfound_view_config
from pyramid.view import exception_view_config
from pyramid.httpexceptions import HTTPBadRequest


@notfound_view_config(renderer='json')
def notfound_view(request):
    request.response.status = 404
    return {'status': request.response.status,
            'message': 'Route not found'}


@exception_view_config(HTTPBadRequest, renderer='json')
def badrequest_view(request):
    request.response.status = 400
    # TODO: write custom messages when 400 exception_response is raised
    msg = 'Your requested seems to be badly formatted. ' \
          'Remember that a simple calculation involves no paranthesis. ' \
          'The operator(s) must have a blank space to its right and left. ' \
          '-- Browse the API documentation at /documentation --'
    return {'status': request.response.status,
            'message': msg}
