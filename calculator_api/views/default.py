from pyramid.view import view_config
from calculator_api.calculate import Calculate
from pyramid.httpexceptions import exception_response


@view_config(route_name='home', openapi=True, renderer='json')
def appinfo_view(request):
    print(request)
    return {'application': 'Calculator API'}


@view_config(route_name='calculate', renderer='json', openapi=True)
def calculate(request):
    if 'input' not in request.json.keys():
        raise exception_response(request=request, status_code=400)
    else:
        input = request.json['input']
        cal = Calculate(calcstr=input)
        if cal.result == 'Error':
            raise exception_response(request=request, status_code=400)
        else:
            return {'calculation string': cal.calcstr,
                    'result': cal.result}
