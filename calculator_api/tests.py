import unittest
import json
import string

from random import choice, randint
from pyramid import testing

from calculator_api.calculate import Calculate


class ViewTests(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp()

    def tearDown(self):
        testing.tearDown()

    def test_appinfo_view(self):
        from .views.default import appinfo_view
        request = testing.DummyRequest()
        info = appinfo_view(request)
        self.assertEqual(info['application'], 'Calculator API')


class FunctionalTests(unittest.TestCase):
    def setUp(self):
        from calculator_api import main
        app = main({})
        from webtest import TestApp
        self.testapp = TestApp(app)

    def test_root(self):
        res = self.testapp.get('/', status=200)
        res_json_body = json.loads(res.body)
        self.assertTrue('application' in res_json_body.keys())

    def test_route_notfound(self):
        rand_str = ''.join([choice(string.ascii_lowercase) for i in
                            range(randint(5, 20))])
        rand_route = f'/{rand_str}'  # random route
        res = self.testapp.get(rand_route, status=404)
        res_json_body = json.loads(res.body)
        self.assertTrue('message' in res_json_body.keys() and 'not found' in
                        res_json_body.get('message'))

    def test_route_calculate(self):
        res = self.testapp.post_json(url='/calculate',
                                     params={'input': '10 + 2'})
        self.assertEqual(res.json.get('result'), 12.0)
        res = self.testapp.post_json(url='/calculate',
                                     params={'input': '10 * 2 / 4'})
        self.assertEqual(res.json.get('result'), 5.0)
        res = self.testapp.post_json(url='/calculate',
                                     params={'input': '(2 * 2) / 3'},
                                     expect_errors=True)
        self.assertEqual(res.json.get('status'), '400 Bad Request')
        res = self.testapp.post_json(url='/calculate',
                                     params={'input': '10 + (2 * 2)'},
                                     expect_errors=True)
        self.assertEqual(res.json.get('status'), '400 Bad Request')
        res = self.testapp.post_json(url='/calculate',
                                     params={'input': '1+1'},
                                     expect_errors=True)
        self.assertEqual(res.json.get('status'), '400 Bad Request')
        res = self.testapp.post_json(url='/calculate',
                                     params={'inputXXX': '10 + (2 * 2)'},
                                     expect_errors=True)
        self.assertEqual(res.json.get('status'), '400 Bad Request')

    def test_calculate_class(self):
        calc = Calculate('10 * 10 / 8.0')
        self.assertEqual(calc.result, 12.5)
        calc = Calculate('1 + 2 * 2')
        self.assertEqual(calc.result, 6.0)
        calc = Calculate('1 + (2 * 2)')
        self.assertEqual(calc.result, 'Error')
