import re
from calculator.simple import SimpleCalculator


class Calculate:
    def __init__(self, calcstr):
        self.calcstr = calcstr
        self.result = None
        self.calculate()
        # TODO: add spaces around operators in self.calcstr misses them.
        #  As to prevent errors on 1+1

    def calculate(self):
        sc = SimpleCalculator()
        sc.run(self.calcstr)
        log_dict = {}
        log_dict_k_v_exp = r'^(?P<k>\w*?(?:\s\w*?)?):\s(?P<v>.+?)$'
        for item in sc.log:
            match = re.match(log_dict_k_v_exp, item)
            if match:
                log_dict[match.groupdict()['k']] = match.groupdict()['v']
        if 'result' not in log_dict.keys() or log_dict['result'] == 'Error':
            self.result = 'Error'
        else:
            self.result = float(log_dict['result'])


if __name__ == '__main__':
    d = Calculate('10 * 10 / 2.0')
    print(d.calcstr, d.result)

    d = Calculate('10 / (2 + 2)')
    print(d.calcstr, d.result)
