FROM python:3.7.6-alpine

RUN adduser -D calc

WORKDIR /home/calc

COPY requirements.txt requirements.txt
RUN python3 -m venv env
RUN env/bin/pip install --upgrade pip setuptools
RUN env/bin/pip install -r requirements.txt

COPY calculator_api calculator_api
COPY setup.py CHANGES.txt runserver.sh .flake8 development.ini MANIFEST.in production.ini pytest.ini README.md tox.ini ./
RUN env/bin/pip install -e ".[testing]"

RUN chown -R calc:calc /home/calc/

USER calc

EXPOSE 6543
ENTRYPOINT ["/home/calc/runserver.sh"]

